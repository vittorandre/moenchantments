package com.biom4st3r.moenchantments;

import com.biom4st3r.moenchantments.enchantments.MoEnchantBuilder;
import com.biom4st3r.moenchantments.enchantments.MoEnchantment;

import net.minecraft.enchantment.Enchantment.Weight;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.AxeItem;
import net.minecraft.item.HoeItem;
import net.minecraft.item.Items;
import net.minecraft.item.MiningToolItem;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.SwordItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class EnchantmentRegistry
{
    public static final String MODID = ModInit.MODID;

    public static final MoEnchantment TREEFELLER = new MoEnchantBuilder(Weight.UNCOMMON, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
        .maxlevel(ModInit.config.TreeFellerMaxBreakByLvl.length)
        .isAcceptible((itemstack)->{return itemstack.getItem() instanceof AxeItem;})
        .minpower((level)->{return level*5;})
        .enabled(ModInit.config.EnableTreeFeller)
        .build("treefeller");
    public static final MoEnchantment VEINMINER = new MoEnchantBuilder(Weight.UNCOMMON, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
        .maxlevel(ModInit.config.VeinMinerMaxBreakByLvl.length)
        .isAcceptible((itemStack)->{return itemStack.getItem() instanceof PickaxeItem;})
        .minpower((level)->{return level*5;})
        .enabled(ModInit.config.EnableVeinMiner)
        .build("veinminer");
    public static final MoEnchantment AUTOSMELT = new MoEnchantBuilder(Weight.VERY_RARE, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
        .addExclusive(Enchantments.SILK_TOUCH)
        .enabled(ModInit.config.EnableAutoSmelt)
        .isAcceptible((itemstack)->{return itemstack.getItem() instanceof MiningToolItem;})
        .addExclusive(Enchantments.SILK_TOUCH)
        .minpower((level)->{return level * 8;})
        .build("autosmelt");
    public static final MoEnchantment TAMEDPROTECTION = new MoEnchantBuilder(Weight.COMMON, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
        .isAcceptible((itemStack)->{return itemStack.getItem() instanceof SwordItem || itemStack.getItem() instanceof AxeItem;})
        .treasure(true)
        .enabled(ModInit.config.EnableTamedProtection)
        .build("tamedprotection");
    public static final MoEnchantment ENDERPROTECTION = new MoEnchantBuilder(Weight.VERY_RARE, EnchantmentTarget.ARMOR, MoEnchantBuilder.ARMOR_SLOTS)
        .maxlevel(3)
        .minpower((level)->{return level*10;})
        .treasure(true)
        .curse(true)
        .enabled(ModInit.config.EnableEnderProtection)
        .build("curseofender");
    public static final MoEnchantment HAVESTER = new MoEnchantBuilder(Weight.UNCOMMON, EnchantmentTarget.DIGGER, EquipmentSlot.MAINHAND)
        .minpower((level)->{return level*4;})
        .isAcceptible((itemStack)->{return itemStack.getItem() instanceof HoeItem;})
        .enabled(false)
        .build("harvester");
    //Gitlab @Mr Cloud
    public static final MoEnchantment SOULBOUND = new MoEnchantBuilder(Weight.UNCOMMON, EnchantmentTarget.ALL, MoEnchantBuilder.ALL_SLOTS)
        .enabled(ModInit.config.EnableSoulBound)
        .maxlevel(ModInit.config.UseStandardSoulboundMechanics ? 1 : ModInit.config.MaxSoulBoundLevel)
        .minpower((level)->{return 20+level;})
        .isAcceptible((itemstack)->{return true;})
        .build("soulbound");
    public static final MoEnchantment POTIONRETENTION = new MoEnchantBuilder(Weight.RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
        .minpower((level)->{return 7*level;})
        .maxlevel(ModInit.config.PotionRetentionMaxLevel)
        .isAcceptible((itemstack)->{return itemstack.getItem() instanceof SwordItem || itemstack.getItem() instanceof AxeItem;})
        .enabled(ModInit.config.EnablePotionRetention)
        .build("potionretension");
    // someone responding to Jeb_ on reddit.
    public static final MoEnchantment BOW_ACCURACY = new MoEnchantBuilder(Weight.RARE, EnchantmentTarget.BOW, MoEnchantBuilder.HAND_SLOTS)
        .maxlevel(2)
        .minpower((level)->{return (level-1)*10;})
        .maxpower((level)->{return (level-1)*10+5;})
        .enabled(ModInit.config.EnableAccuracy && !Plugin.extraBowsFound)
        .build("bowaccuracy");
    public static final MoEnchantment BOW_ACCURACY_CURSE = new MoEnchantBuilder(Weight.RARE, EnchantmentTarget.BOW, MoEnchantBuilder.HAND_SLOTS)
        .maxlevel(1)
        .curse(true)
        .addExclusive(BOW_ACCURACY)
        .enabled(ModInit.config.EnableInAccuracy && !Plugin.extraBowsFound)
        .build("bowinaccuracy");
    // Gitlab @cryum
    public static final MoEnchantment ARROW_CHAOS = new MoEnchantBuilder(Weight.VERY_RARE,EnchantmentTarget.BOW,MoEnchantBuilder.HAND_SLOTS)
        .curse(true)
        .minpower((level)->{return 30;})
        .enabled(ModInit.config.EnableArrowChaos && !Plugin.extraBowsFound)
        .build("arrow_chaos");
    public static final MoEnchantment BUILDERS_WAND = new MoEnchantBuilder(Weight.UNCOMMON, EnchantmentTarget.ALL, MoEnchantBuilder.HAND_SLOTS)
        .treasure(true)
        .minpower((level)->{return 10;})
        .enabled(false)
        .isAcceptible((itemstack)->{return itemstack.getItem() == Items.STICK;})
        .build("builders_wand");
        //https://www.reddit.com/r/minecraftsuggestions/comments/dx8zom/shield_enchantments/
        public static final MoEnchantment SHIELD_REFLECT = new MoEnchantBuilder(Weight.UNCOMMON, EnchantmentTarget.ALL, EquipmentSlot.OFFHAND)
        .maxlevel(3)
        .treasure(true)
        .build("reflectarrow");
    public static final MoEnchantment TRAINING_WEAPON = new MoEnchantBuilder(Weight.RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
        .maxlevel(1)
        .treasure(true)
        .enabled(false)//ModInit.config.EnableTrainingWeapon)
        .build("training_weapon"); // Provide more xp from kill. but reduce damage;
    public static final MoEnchantment VAMPIRISM = new MoEnchantBuilder(Weight.VERY_RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
        .treasure(true)
        .minpower((level)-> {return 10;})
        .maxpower((level)->{return 15;})
        .enabled(false)//ModInit.config.EnableVampirism)
        .build("vampirism");
    public static final MoEnchantment HOARDING = new MoEnchantBuilder(Weight.VERY_RARE, EnchantmentTarget.WEAPON, EquipmentSlot.MAINHAND)
        .curse(true)
        .minpower((level)->{return 20;})
        .maxpower((level)->{return 30;})
        .enabled(false)//ModInit.config.EnableHording)
        .build("hording");
    public static final MoEnchantment SHIELD_BASH = new MoEnchantBuilder(Weight.UNCOMMON, EnchantmentTarget.WEARABLE, MoEnchantBuilder.HAND_SLOTS)
        .treasure(true)
        .minpower((level)->{return 10;})
        .enabled(false)
        .addExclusive(SHIELD_REFLECT)
        .build("shield_bash");
        
    public static void init()
    {
        for(MoEnchantment e : new MoEnchantment[] {
            TREEFELLER,VEINMINER,AUTOSMELT,
            TAMEDPROTECTION,ENDERPROTECTION,POTIONRETENTION,
            HAVESTER,SOULBOUND,BOW_ACCURACY,
            BOW_ACCURACY_CURSE,ARROW_CHAOS,TRAINING_WEAPON,
            VAMPIRISM,HOARDING,SHIELD_REFLECT,SHIELD_BASH,
            BUILDERS_WAND,})
        {
            if(e.enabled())
                Registry.register(Registry.ENCHANTMENT, new Identifier(MODID, e.regName().toLowerCase()), e);
        }
    }
}