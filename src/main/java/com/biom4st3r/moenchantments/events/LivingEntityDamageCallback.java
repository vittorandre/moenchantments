package com.biom4st3r.moenchantments.events;

import com.biom4st3r.moenchantments.logic.EnderProtectionLogic;
import com.biom4st3r.moenchantments.logic.FamiliaryLogic;
import com.biom4st3r.moenchantments.logic.HoardingLogic;

import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.impl.base.event.EventFactoryImpl;
import net.minecraft.entity.Entity;
import net.minecraft.entity.damage.DamageSource;

/**
 * LivingEntityDamageCallback
 */
@FunctionalInterface
public interface LivingEntityDamageCallback {

    Event<LivingEntityDamageCallback> EVENT = EventFactoryImpl.createArrayBacked(LivingEntityDamageCallback.class, 
        (listeners)-> (damageSource_1, float_1, e,  ci)->
        {
            for(LivingEntityDamageCallback callback : listeners)
            {
                callback.onDamage(damageSource_1, float_1, e, ci);
            }
        });

    void onDamage(DamageSource damageSource, float damage, Entity entity, CallbackInfoReturnable<Boolean> ci);

    public static void init()
    {
		FamiliaryLogic.init();
		EnderProtectionLogic.init();
		HoardingLogic.init();
    }

}