package com.biom4st3r.moenchantments.interfaces;

import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;


public interface PotionEffectRetainer
{
    
    public void setPotionEffectAndCharges(StatusEffectInstance effect, int val);

    public void setPotionEffectAndCharges(int effect,int ampifier, int val);

    public int getAmplification();

    public void addCharges(int val);

    public StatusEffect getEffect();

    public int getCharges();

    public StatusEffectInstance useEffect();
    
    public int isPotionRetainer();

    public void updateLore();

    public int getMaxCharges();

    public PotionEffectRetainer getRetainer();

    public void removeEffect();
    

}