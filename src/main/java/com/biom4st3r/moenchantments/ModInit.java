package com.biom4st3r.moenchantments;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.biom4st3r.biow0rks.BioLogger;
import com.biom4st3r.moenchantments.events.LivingEntityDamageCallback;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import net.fabricmc.api.ModInitializer;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModInit implements ModInitializer {
	public static final String MODID = "biom4st3rmoenchantments";
	public static MoEnchantsConfig config;
	public static BioLogger logger = new BioLogger("MoEnchantments");

	public static final UUID uuidZero = new UUID(0L, 0L);
	public static IntArrayList int_block_whitelist = new IntArrayList(100);
	public static List<Item> autoSmelt_blacklist = new ArrayList<Item>();
	public static boolean lockAutoSmeltSound = false;

	@Override
	public void onInitialize() {
		
		config = MoEnchantsConfig.init(config);
		EnchantmentRegistry.init();
		LivingEntityDamageCallback.init();
		blacklistAutosmelt();
	}

	public static void whitelistToBlock() {
		for (String s : config.veinMinerBlockWhiteList) {
			Block block = Registry.BLOCK.get(new Identifier(s));
			if (block != Blocks.AIR) 
			{
				int_block_whitelist.add(Registry.BLOCK.getRawId(block));
				logger.log("added %s as %s", block.getDropTableId().toString(),Registry.BLOCK.getRawId(block));
				continue;
			}
			else
			{
				logger.log("%s was not found", s );
			}
		}
	}

	public static void blacklistAutosmelt()
	{
		for(String s : config.AutoSmeltBlackList)
		{
			Item i = Registry.ITEM.get(new Identifier(s));
			if(i != Items.AIR)
			{
				autoSmelt_blacklist.add(i);
				logger.log("added %s to autosmelt blacklist",s);
			}
			else
			{
				logger.log("%s was not found", s);
			}
		}
	}
}