package com.biom4st3r.moenchantments.mixin;

import com.biom4st3r.moenchantments.events.LivingEntityDamageCallback;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;

@Mixin(LivingEntity.class)
public abstract class LivingEntityDamageEventMxn
{
    @Inject(at = @At("HEAD"), method = "damage", cancellable = true)
    public void LivingEntityEvent(DamageSource damageSource_1, float float_1, CallbackInfoReturnable<Boolean> ci)
    {
        LivingEntityDamageCallback.EVENT.invoker().onDamage(damageSource_1, float_1, (Entity)(Object)this, ci);
    }

}