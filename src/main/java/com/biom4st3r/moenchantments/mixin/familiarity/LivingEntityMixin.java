package com.biom4st3r.moenchantments.mixin.familiarity;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.enchantments.MoEnchantment;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

@Mixin(LivingEntity.class)
public abstract class LivingEntityMixin extends Entity  {
    protected LivingEntityMixin(EntityType<?> entityType_1, World world_1) {
        super(entityType_1, world_1); 
    }

    @Inject(at = @At("HEAD"), method = "damage", cancellable = true)
    public void familiarityEnchantmentImpl(DamageSource damageSource_1, float float_1, CallbackInfoReturnable<Boolean> ci) 
    {
        if (((Entity) this) instanceof TameableEntity && damageSource_1.getAttacker() instanceof PlayerEntity) 
        {
            PlayerEntity attacker = (PlayerEntity) damageSource_1.getAttacker();
            TameableEntity defender = (TameableEntity) (Entity) this;
            if (MoEnchantment.hasEnchant(EnchantmentRegistry.TAMEDPROTECTION, attacker.getMainHandStack())
                    && defender.getOwnerUuid() != ModInit.uuidZero) 
                    {
                if (ModInit.config.TameProtectsOnlyYourAnimals) 
                {
                    if (defender.isOwner(attacker)) {
                        ci.setReturnValue(false);
                    }
                } 
                else 
                {
                    ci.setReturnValue(false);
                }
            }
        }
    }
}