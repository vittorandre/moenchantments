package com.biom4st3r.moenchantments.mixin.arrowchaos;

import com.biom4st3r.moenchantments.logic.ChaosArrowLogic;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.entity.projectile.ProjectileEntity.PickupPermission;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

@Mixin(BowItem.class)
public abstract class BowItemMxn
{

    @Inject(at = @At(value = "INVOKE_ASSIGN",target = "net/minecraft/item/ArrowItem.createArrow(Lnet/minecraft/world/World;Lnet/minecraft/item/ItemStack;Lnet/minecraft/entity/LivingEntity;)Lnet/minecraft/entity/projectile/ProjectileEntity;"),
        method="onStoppedUsing",
        locals = LocalCapture.CAPTURE_FAILHARD)
    public void addChaosArrow(ItemStack stack, World world, LivingEntity user, int remainingUseTicks, CallbackInfo ci,PlayerEntity playerEntity, boolean bl, ItemStack itemStack, int i, float f, boolean bl2, ArrowItem arrowItem, ProjectileEntity projectileEntity)
    {
        if(ChaosArrowLogic.makePotionArrow(user, projectileEntity, user.getRandom()))
        {
            projectileEntity.pickupType = PickupPermission.CREATIVE_ONLY;
        }

    }

}