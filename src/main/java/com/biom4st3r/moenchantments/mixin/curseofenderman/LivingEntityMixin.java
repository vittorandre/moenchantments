package com.biom4st3r.moenchantments.mixin.curseofenderman;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.logic.EnderProtectionLogic;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.world.World;

@Mixin(LivingEntity.class)
public abstract class LivingEntityMixin extends Entity {
    protected LivingEntityMixin(EntityType<?> entityType_1, World world_1) {
        super(entityType_1, world_1);
        
    }

    @Inject(at = @At("HEAD"), method = "damage", cancellable = true)
    public void enderProtection(DamageSource damageSource_1, float float_1, CallbackInfoReturnable<Boolean> ci) 
    {
        int enderProLvl = EnchantmentHelper.getEquipmentLevel(EnchantmentRegistry.ENDERPROTECTION,
                ((LivingEntity) (Object) this));
        if (enderProLvl > 0) {
            if (EnderProtectionLogic.doLogic(damageSource_1, enderProLvl, (LivingEntity) (Object) this)) {
                ci.setReturnValue(false);
            }
        }
    }
}