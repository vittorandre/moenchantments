package com.biom4st3r.moenchantments.logic;

import java.util.List;
import java.util.Optional;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.interfaces.PlayerExperienceStore;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.BasicInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.LootTable;
import net.minecraft.loot.LootTables;
import net.minecraft.loot.context.LootContext;
import net.minecraft.loot.context.LootContextParameters;
import net.minecraft.loot.context.LootContextTypes;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.recipe.RecipeType;
import net.minecraft.recipe.SmeltingRecipe;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Identifier;

/**
 * AlphafireLogic
 */
public class AlphafireLogic {

    public static void doLogic(BlockState blockState, LootContext.Builder lootBuilder, Block block, CallbackInfoReturnable<List<ItemStack>> ci)
    {

        ItemStack tool = lootBuilder.get(LootContextParameters.TOOL);
        Identifier loottableId = block.getDropTableId();
        if(EnchantmentHelper.getLevel(EnchantmentRegistry.AUTOSMELT, tool) > 0 && loottableId != LootTables.EMPTY)
        {
            Entity player = lootBuilder.get(LootContextParameters.THIS_ENTITY);
            lootBuilder.put(LootContextParameters.BLOCK_STATE, blockState);
            ServerWorld serverworld = lootBuilder.getWorld();
            LootTable loottTable = serverworld.getServer().getLootManager().getSupplier(loottableId);
            List<ItemStack> stacks = loottTable.getDrops(lootBuilder.build(LootContextTypes.BLOCK));
            RecipeManager rm = player.world.getRecipeManager(); 
            Inventory basicInv = new BasicInventory();
            
            ItemStack itemToBeChecked = ItemStack.EMPTY;
            Optional<SmeltingRecipe> smeltingResult;
            for(int stacksIndex = 0; stacksIndex < stacks.size(); stacksIndex++)
            {
                itemToBeChecked = stacks.get(stacksIndex);
                basicInv = new BasicInventory(itemToBeChecked);
                smeltingResult = rm.getFirstMatch(RecipeType.SMELTING, basicInv, player.world);
                if(smeltingResult.isPresent() && !(ModInit.autoSmelt_blacklist.contains(itemToBeChecked.getItem())))
                {
                    stacks.set(stacksIndex, new ItemStack(smeltingResult.get().getOutput().getItem(),itemToBeChecked.getCount()));
                    ((PlayerExperienceStore)(Object)player).addExp(smeltingResult.get().getExperience() * itemToBeChecked.getCount());
                    if(!ModInit.lockAutoSmeltSound)
                    {
                        ModInit.lockAutoSmeltSound = true;
                        serverworld.playSound((PlayerEntity)null, player.getBlockPos(), SoundEvents.BLOCK_FIRE_EXTINGUISH, SoundCategory.BLOCKS, 0.1f, 0.1f);
                    }
                }
            }
            ci.setReturnValue(stacks);
        }

    }

}