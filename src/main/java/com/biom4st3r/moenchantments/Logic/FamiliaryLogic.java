package com.biom4st3r.moenchantments.logic;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.enchantments.MoEnchantment;
import com.biom4st3r.moenchantments.events.LivingEntityDamageCallback;

import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;

/**
 * FamiliaryLogic
 */
public class FamiliaryLogic {

    public static void init()
    {
        LivingEntityDamageCallback.EVENT.register((damageSource,damage,entity,ci)->
        {
            if ((entity) instanceof TameableEntity && damageSource.getAttacker() instanceof PlayerEntity) 
            {
                PlayerEntity attacker = (PlayerEntity) damageSource.getAttacker();
                TameableEntity defender = (TameableEntity) entity;
                if (MoEnchantment.hasEnchant(EnchantmentRegistry.TAMEDPROTECTION, attacker.getMainHandStack())
                        && defender.getOwnerUuid() != ModInit.uuidZero) 
                        {
                    if (ModInit.config.TameProtectsOnlyYourAnimals) 
                    {
                        if (defender.isOwner(attacker)) {
                            ci.setReturnValue(false);
                        }
                    } 
                    else 
                    {
                        ci.setReturnValue(false);
                    }
                }
            }
        });
    }
}